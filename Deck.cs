﻿using System;

namespace TextGolf {
    public class Deck {
        private Card[] deck;
        private int top = 0;

        public int Count {
            private set;
            get;
        }

        public Deck(int decks = 1, bool useJokers = false) {
            Count = decks * 52;
            if(useJokers) {
                Count += 2 * decks;
            }
            deck = new Card[Count];

            for(int i = 0; i < Count; i++) {
                deck[i] = new Card(i);
            }

            if(useJokers) { //add 2 jokers/deck
                for(int i = 0; i < 2 * decks; i++) {
                    deck[(Count - 1) - i] = Card.makeJoker();
                }
            }
        }

        /// <summary>
        /// Fisher-Yates shuffle, O(n), unbiased permutation.
        /// </summary>
        public void shuffle() {
            Card swap;
            Random rand = new Random();

            for(int i = 0; i < Count - 2; i++) {
                int j = rand.Next(i, Count);
                swap = deck[i];
                deck[i] = deck[j];
                deck[j] = swap;
            }

            top = 0; //deck is reset
        }

        /// <summary>
        /// Draw one card from the top of the deck.
        /// </summary>
        public Card drawCard() {
            if(top < Count) {
                return deck[top++];
            } else {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Draw a set of cards.
        /// </summary>
        /// <param name="cards">Cards to be drawn.</param>
        public Card[] drawSet(int cards) {
            Card[] set = new Card[cards];

            for(int i = 0; i < cards; i++) {
                if(top < Count) {
                    set[i] = deck[top++];
                } else {
                    throw new InvalidOperationException();
                }
            }

            return set;
        }

        /// <summary>
        /// Prints the remaining deck to the console in rows of 13 cards, deck must be shuffled after.
        /// </summary>
        public void PrintDeck() {
            for(int i = 0; i < Count; i++) {
                if(i % 13 == 0 && i > 0) {
                    Console.WriteLine();
                }
                Console.Write(drawCard() + " ");
            }

        }
    }
}
