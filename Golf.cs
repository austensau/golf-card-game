﻿using System;

namespace TextGolf {
    public enum CardValues {
        A = 1,
        J = 10,
        Q = 10,
        K = 0,
        JOKER = -2
    }

    public class Golf {
        private Deck deck;
        private Card discard;
        private Player[] players;
        private bool roundOver;

        public int PlayerCount {
            get {
                return players.Length;
            }
        }

        public int Rounds {
            private set; get;
        }

        public Golf(int players = 3, int humans = 1, int rounds = 9) {
            if(players < 2 || players > 10 || humans < 0 || humans > 10) {
                throw new ArgumentOutOfRangeException();
            }

            this.players = new Player[players];
            initPlayers(humans);
            CreateDeck(players);
            Rounds = rounds;
        }

        public void startGame() {
            for(int i = 0; i < Rounds; i++) {
                startRound(i);
            }
        }

        public void startRound(int round) {
            roundOver = false;
            deck.shuffle();
            discard = deck.drawCard();

            for(int j = 0; j < PlayerCount; j++) {
                players[j].setNewHand(deck.drawSet(6));
            }

            int i = 0;
            for(; !roundOver; i++) { //take turns
                Console.WriteLine("Player " + ((i % PlayerCount) + 1) + "'s turn.\n");
                if(i < PlayerCount) { //flip two cards on first turn
                    players[i].flipCards();
                    printBoard();
                    Console.WriteLine();
                }
                players[i % PlayerCount].takeTurn(deck, ref discard, out roundOver);
                printBoard();
                Console.WriteLine();
            }

            //round almost over, finish last turn
            for(int j = i % PlayerCount, p = 0; p < PlayerCount - 1; j++, p++) {
                Console.WriteLine("Player " + ((j % PlayerCount) + 1) + "'s turn.\n");
                players[j % PlayerCount].takeTurn(deck, ref discard, out roundOver);
                printBoard();
                Console.WriteLine();
            }

            //TODO flip remaining cards

            //print scores for round
            Console.WriteLine("Round " + (round + 1) + " results.");
            for(int j = 0; j < PlayerCount; j++) {
                players[j].calculateScore();
                Console.Write("P" + (j + 1) + " score: " + players[j].Score + "\t");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Print each players hand
        /// </summary>
        public void printBoard() {
            Card[][] hands = new Card[PlayerCount][];

            for(int i = 0; i < PlayerCount; i++) { //get all hands, write names
                hands[i] = players[i].getHand();
                Console.Write("Player " + (i + 1) + "\t");
            }
            Console.WriteLine();

            for(int i = 0; i < 2; i++) { //players have 2 rows of cards
                for(int j = 0; j < 3 * PlayerCount; j++) { //3 * numPlayers cards per row
                    int rowOffset = 0;
                    if(i == 1) {
                        rowOffset = 3;
                    }
                    Card card = hands[j / 3][(j % 3) + rowOffset];

                    if(j % 3 == 0 && j > 0) {
                        Console.Write("\t");
                    }
                    if(card != null) {
                        Console.Write(card + " ");
                    } else {
                        Console.Write("?? ");
                    }
                }
                Console.WriteLine();
            }
        }

        public static int getFaceValue(Face face) {
            return (int)(CardValues)Enum.Parse(typeof(CardValues), face.ToString());
        }

        private void initPlayers(int humans) {
            for(int i = 0; i < humans; i++) { //init humans
                this.players[i] = new Player(true);
            }
            for(int i = humans; i < PlayerCount; i++) { //init AI
                this.players[i] = new Player(false);
            }
        }

        private void CreateDeck(int players) {
            if(players < 4) { //init decks w/jokers
                deck = new Deck(1, true); //2 to 3 players use one deck
            } else {
                deck = new Deck(2, true); //4+ use two decks
            }
        }
    }
}
