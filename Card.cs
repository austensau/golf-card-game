﻿using System;
using System.Text;

namespace TextGolf {
    public enum Suit {
        heart = 3,
        diamond = 4,
        club = 5,
        spade = 6,
        JOKER = 7
    }
    public enum Face {
        A = 1,
        J = 11,
        Q = 12,
        K = 13,
        JOKER = 14
    }
    public class Card {
        public Suit suit {
            private set; get;
        }

        public Face face {
            private set; get;
        }

        public Card(int i) {
            i %= 52; //prevent wrong suit
            suit = (Suit)(i / 13) + 3;
            face = (Face)(i % 13) + 1;
        }

        private Card() {
            
        }


        public static Card makeJoker() {
            Card joker = new Card();
            joker.suit = Suit.JOKER;
            joker.face = Face.JOKER;

            return joker;
        }

        /// <summary>
        /// Return "\uSuit" + faceValue using unicode suit symbols
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            Console.OutputEncoding = Encoding.Unicode;
            switch(suit) {
                case Suit.heart:
                    return "\u2665" + face;
                case Suit.diamond:
                    return "\u2666" + face;
                case Suit.club:
                    return "\u2663" + face;
                case Suit.spade:
                    return "\u2660" + face;
                default:
                    return "JK";
            }
            
        }
    }
}
