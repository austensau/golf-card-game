﻿using System;

namespace TextGolf {
    public class Player {
        private Card[] hand;
        private int score = 0;
        private bool[] flipped = new bool[6];

        public int Score {
            get {
                return score;
            }
        }

        public bool isHuman {
            private set; get;
        }

        public Player(bool humanPlayer) {
            isHuman = humanPlayer;
        }

        public void takeTurn(Deck deck, ref Card discard, out bool roundOver) {
            if(!isHuman) {
                computerTakeTurn(deck, ref discard);
            } else {
                string choice;
                Card drawnCard = drawFromPile(deck, discard, out choice);
                discard = replaceCard(drawnCard, choice);
            }
            if(allCardsFlipped()) {
                roundOver = true;
            } else {
                roundOver = false;
            }
        }

        /// <summary>
        /// Player flips their first two cards of their choice.
        /// </summary>
        public void flipCards() {
            if(isHuman) {
                string[] instr = new string[] { "first", "second" };
                string choice;

                Console.WriteLine("Choose two cards to flip.");
                for(int i = 0; i < 2; i++) {
                    Console.Write("Choose the " + instr[i] + " card(1-6): ");
                    choice = Console.ReadLine(); //TODO input validation
                    int num = Convert.ToInt32(choice) - 1;
                    if(!flipped[num]) {
                        flipped[num] = true;
                        Console.WriteLine("You flipped " + hand[num]);
                    } else {
                        Console.WriteLine("Second choice must be different from the first, try again.");
                        i--; //redo second pick
                    }

                }
            } else {
                Random rand = new Random();
                flipped[rand.Next() % 6] = true; //flip random card
                int secondCard = rand.Next() % 6;
                while(flipped[secondCard]) { //pick unique 2nd card
                    secondCard = rand.Next() % 6;
                }
                flipped[secondCard] = true;

            }
        }

        /// <summary>
        /// Returns the players hand. Face down cards are null.
        /// </summary>
        public Card[] getHand() {
            Card[] displayed = new Card[hand.Length];

            for(int i = 0; i < hand.Length; i++) {
                if(flipped[i]) {
                    displayed[i] = hand[i];
                }
            }

            return displayed;
        }

        public void setNewHand(Card[] newHand) {
            hand = newHand;
            flipped = new bool[6]; //all cards facedown
        }

        public void calculateScore() {
            if(allCardsFlipped()) {
                for(int i = 0; i < 3; i++) {
                    if(hand[i].face != hand[i + 3].face) { //if not a match, add card values to score.
                        score += Golf.getFaceValue(hand[i].face);
                        score += Golf.getFaceValue(hand[i + 3].face);
                    }
                }
            }
        }

        /// <summary>
        /// Player draws a card from either the deck or the discard pile.
        /// </summary>
        private Card drawFromPile(Deck deck, Card discard, out string choice) {
            Console.Write("Draw from deck (d) or discard pile (p)? Discard is " + discard + ": ");
            choice = Console.ReadLine();
            if(choice.ToLower() == "d") {
                return deck.drawCard();
            } else if(choice.ToLower() == "p") {
                return discard;
            } else {
                Console.WriteLine("Invalid choice, try again.");
                return drawFromPile(deck, discard, out choice);
            }
        }

        /// <summary>
        /// Player chooses to either discard the drawn card (if not from discard pile) or discards a facedown card.
        /// </summary>
        private Card replaceCard(Card drawnCard, string pile) {
            Console.Write("You drew " + drawnCard + " which card will you replace? (drawn(0), hand(1-6)): ");
            string choice = Console.ReadLine();
            int num = Convert.ToInt32(choice); // TODO input validation
            if(num == 0 && pile.ToLower() != "pile") { //Can't discard a card drawn from the discard pile
                return drawnCard;
            } else if(num > 0 && !flipped[num - 1]) { //can't discard a card that's already flipped
                Card swapCard = hand[num - 1];
                hand[num - 1] = drawnCard;
                flipped[num - 1] = true;
                return swapCard;
            } else { //invalid input
                if(num == 0) {
                    Console.WriteLine("You must use cards taken from the discard pile, try again.");
                } else {
                    Console.WriteLine("Can't discard a card that's already flipped, try again.");
                }
                return replaceCard(drawnCard, pile);
            }
        }

        private Card computerTakeTurn(Deck deck, ref Card discard) {
            Card compare = discard;
            Card swap;
            for(int i = 0; i < 2; i++) {
                for(int j = 0; j < 6; j++) { //attempt to find a match from discard, then deck
                    //basic matching logic
                    if(hand[j].face == compare.face && flipped[j] && !flipped[(j + 3) % 6]) {
                        swap = hand[(j + 3) % 6];
                        hand[(j + 3) % 6] = compare;
                        flipped[(j + 3) % 6] = true;
                        return swap;
                    }
                }
                compare = deck.drawCard();
            }
            if(Golf.getFaceValue(compare.face) < 7) { //use only 'good' cards
                int smallest = 11;
                int smallestIndex = -1;

                for(int i = 0; i < 6; i++) { //prioritize swap w/unflipped column
                    if(!flipped[i] && !flipped[(i + 3) % 6]) {
                        swap = hand[i];
                        flipped[i] = true;
                        hand[i] = compare;
                        return swap;
                    } //find smallest value card with unflipped card below it
                    if(flipped[i] && !flipped[(i + 3) % 6] && Golf.getFaceValue(hand[i].face) < smallest) { 
                        smallest = Golf.getFaceValue(hand[i].face);
                        smallestIndex = i;
                    }
                } //else place next to card with smallest face value
                swap = hand[(smallestIndex + 3) % 6];
                flipped[(smallestIndex + 3) % 6] = true;
                hand[(smallestIndex + 3) % 6] = compare;
                return swap;
            } else { //'bad' card
                return compare;
            }
        }

        private bool allCardsFlipped() {
            bool allFlipped = true;
            for(int i = 0; i < flipped.Length; i++) {
                allFlipped &= flipped[i]; //and all values together
            }
            return allFlipped;
        }
    }
}
